//////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define EPOCH  1390357567

struct t
{
unsigned int Years;
unsigned int Days;
unsigned int Hours;
unsigned int Minutes;
unsigned int Seconds;
time_t start;
time_t end;
time_t diff;

};

int main() {

struct t countdown;


printf("Reference time: Wed Jan 21 04:26:07 PM HST 2014\n");
//countdown 
while(1)
{
countdown.start =  EPOCH; //Wed Jan 21 04:26:07 PM HST 2014"
countdown.end =  time(NULL); //current time
countdown.diff = countdown.end - countdown.start;

//conversions from seconds
countdown.Years = countdown.diff/31622400; 
countdown.Days = (countdown.diff % 31622400)/86400;
countdown.Hours = ((countdown.diff % 31622400) % 86400)/3600;
countdown.Minutes = (((countdown.diff % 31622400) % 86400)% 3600)/60; 
countdown.Seconds = (((countdown.diff % 31622400) % 86400)% 3600)%60;
printf("Years:%d   Days:%d   Hours:%d   Minutes:%d   Seconds:%d \n ", countdown.Years, countdown.Days, countdown.Hours, countdown.Minutes, countdown.Seconds);

sleep(1);
}



}



